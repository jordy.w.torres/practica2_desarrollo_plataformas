const URL = "https://fakerestapi.azurewebsites.net/api/v1/Activities"

export const Activity = async () => {
    const datos = await (await fetch(URL, {
        method: "GET"
    })).json();
    return datos;
};

export const GuardarActivity = async (data) => {
    const headers = {
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URL, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json()
    return datos;
};

export const ModificarActividad = async (id, data) => {
    const headers = {
        "Content-Type": "application/json",
    }; 
    const response = await fetch(`${URL}/${id}`, {
        method: "PUT",
        headers: headers,
        body: JSON.stringify(data),
    });
    const result = await response.json();
   
    return result;
};

export const ObtenerDatos = async (nroFila) => {
    const datos = await (await fetch(URL + "/" + nroFila, {
        method: "GET",
    })).json();
  
    return datos;
};

export const GetDatos = async (nroFila) => {
    const response = await fetch(URL + "/" + nroFila, {
        method: "GET",
    });
    const result = await response.json();
    return result;
};
