import React from 'react';
import './App.css';
import { Route, Routes} from 'react-router-dom';
import Actividades from './fragment/Actividad';
function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Actividades/>}/>
      </Routes>
      </div>
  );
}

export default App;