import logo from './../logo.svg';
import './../App.css';

const Principal = () => {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p><code></code></p>
                <a className="App-link" href={"/PresentarActividades"} rel="noopener noreferrer"></a>
            </header>
        </div >
    );
}

export default Principal;