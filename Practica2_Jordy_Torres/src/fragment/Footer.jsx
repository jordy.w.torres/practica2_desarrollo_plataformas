const Footer = () => {
    const footerStyle = {
        backgroundColor: "rgba(0, 0, 0, 0)"
    };
    return (
        <footer className="text-center text-lg-start bg-light text-muted">
            <div className="text-center p-4" style={footerStyle}>
                <a className="text-reset fw-bold" href="#"></a>
            </div>
        </footer>
    );
}

export default Footer;